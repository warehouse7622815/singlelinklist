#include<iostream>
#include"SingleLinkList.h"
using namespace std;

CSingleLinkList::CSingleLinkList()
{
    InitLinkList();
}

CSingleLinkList::~CSingleLinkList()
{
    Destroy();
}

/******************************************************************
* @brief InitLinkList          初始化链表
* Detail                       指定头结点的值并让其指向NULL                              
*
* @param     
* @return
* @note
******************************************************************/
void CSingleLinkList::InitLinkList()
{
    m_pHeadNode = new SLinkListNode;

    if (!m_pHeadNode)
    {
        cout << "内存分配失败。" << endl;
        exit(1);
    }

    m_pHeadNode->iNodeData = 0;           //头结点记录链表结点数
    m_pHeadNode->pNextNode = NULL;        //避免出现野指针
}

/******************************************************************
* @brief Destroy               销毁链表
* Detail                       释放单链表，最后将头结点指向NULL                              
*
* @param
* @return
* @note
******************************************************************/
void CSingleLinkList::Destroy()
{
    SLinkListNode* pLinkList = m_pHeadNode;

    while (pLinkList)
    {
        SLinkListNode* pDelNode = pLinkList;
        pLinkList               = pLinkList->pNextNode;

        delete pDelNode;
        pDelNode = NULL;
    }

    m_pHeadNode = NULL;
}

/******************************************************************
* @brief PrintLinkList         打印链表的值
* Detail                       链表为空输出提示，不为空时逐结点输出
* 
* @param
* @return 
* @note
******************************************************************/
void CSingleLinkList::PrintLinkList()
{
    if (0 == m_pHeadNode->iNodeData)
    {
        cout << "该链表为空，无值打印" << endl;
    }
    else
    {
        SLinkListNode* pPrintNodeData = m_pHeadNode->pNextNode;

        while (pPrintNodeData != NULL)
        {
            cout << pPrintNodeData->iNodeData << "    ";
            pPrintNodeData = pPrintNodeData->pNextNode;
        }
        cout << endl;
    }
}

/******************************************************************
* @brief InsertHeadNode        头插法
* Detail                       用头插法给链表进行“增”操作，头结点
*                              会记录链表中的结点数
*
* @param[in] iNodeData         插入结点的数值
* @return                      
* @note
******************************************************************/
void CSingleLinkList::InsertHeadNode(const int iNodeData)
{
    SLinkListNode* pStorageNodeData = new SLinkListNode;

    if (!pStorageNodeData)
    {
        cout << "内存分配失败。" << endl;
        exit(1);
    }

    pStorageNodeData->iNodeData = iNodeData;
    pStorageNodeData->pNextNode = m_pHeadNode->pNextNode;
    m_pHeadNode->pNextNode      = pStorageNodeData;
    m_pHeadNode->iNodeData++;

    cout << "已将" << iNodeData << "以头插法插入链表" << endl;
}

/******************************************************************
* @brief InsertTailNode        尾插法
* Detail                       用尾插法给链表进行“增”操作，头结点
*                              会记录链表中的结点数
*
* @param[in] iNodeData         插入结点的数值
* @return                      
* @note
******************************************************************/
void CSingleLinkList::InsertTailNode(const int iNodeData)
{
    SLinkListNode* pStorageNodeData = new SLinkListNode;

    if (!pStorageNodeData)
    {
        cout << "内存分配失败。" << endl;
        exit(1);
    }

    pStorageNodeData->iNodeData  = iNodeData;
    pStorageNodeData->pNextNode  = NULL;
    SLinkListNode* pTailLinkList = m_pHeadNode;

    while (pTailLinkList->pNextNode != NULL)
    {
        pTailLinkList = pTailLinkList->pNextNode;
    }

    pTailLinkList->pNextNode = pStorageNodeData;
    m_pHeadNode->iNodeData++;

    cout << "已将" << iNodeData << "以尾插法插入链表" << endl;
}

/******************************************************************
* @brief DelHeadNode           头删法
* Detail                       用头删法给链表进行“删”操作，链表为
*                              空时会提示并返回空链表。头结点会记录
*                              链表中的结点数
* 
* @param
* @return                      
* @note
******************************************************************/
void CSingleLinkList::DelHeadNode()
{
    if (0 == m_pHeadNode->iNodeData)
    {
        cout << "传入的链表为空，无法完成头删操作" << endl;
    }
    else
    {
        SLinkListNode* pStorageDelNode = m_pHeadNode->pNextNode;
        m_pHeadNode->pNextNode         = pStorageDelNode->pNextNode;
        m_pHeadNode->iNodeData--;

        //先释放结点再指向空(不可交换操作顺序)
        delete pStorageDelNode;  
        pStorageDelNode = NULL;
        
        cout << "已以头删法删除链表一个结点" << endl;
    }  
}

/******************************************************************
* @brief DelTailNode           尾删法
* Detail                       用尾删法给链表进行“删”操作，链表为
*                              空时会提示并返回空链表。头结点会记录
*                              链表中的结点数
*
* @param
* @return                      
* @note
******************************************************************/
void CSingleLinkList::DelTailNode()
{
    if (0 == m_pHeadNode->iNodeData)
    {
        cout << "传入的链表为空，无法完成尾删操作" << endl;
    }
    else
    {
        SLinkListNode* pSecondToTailOfLinkList = m_pHeadNode;
        SLinkListNode* pTailOfLinkList         = pSecondToTailOfLinkList->pNextNode;

        while (pTailOfLinkList->pNextNode != NULL)
        {            
            pSecondToTailOfLinkList = pSecondToTailOfLinkList->pNextNode;
            pTailOfLinkList         = pTailOfLinkList->pNextNode;
        }

        pSecondToTailOfLinkList->pNextNode = NULL;
        m_pHeadNode->iNodeData--;

        delete pTailOfLinkList;
        pTailOfLinkList = NULL;

        cout << "已以尾删法删除链表一个结点" << endl;
    }
}

/******************************************************************
* @brief FindNodeByData        按值查询链表中的数据
* Detail                       根据传入的数值，逐结点查询链表。查询
*                              失败返回NULL，查询成功返回查询到的结
*                              点。
*
* @param[in] iFindData         需要查询的数值
* @return                      返回查询到的结点
* @note
******************************************************************/
SLinkListNode* CSingleLinkList::FindNodeByData(const int iFindData)
{
    SLinkListNode* pFindNodeByData = m_pHeadNode->pNextNode;

    while ((pFindNodeByData != NULL) && (pFindNodeByData->iNodeData != iFindData))
    {
        pFindNodeByData = pFindNodeByData->pNextNode;
    }

    if (pFindNodeByData != NULL)
    {
        cout << "已在链表找到" << iFindData << "的相应结点。" << endl;
        return pFindNodeByData;
    }
    else
    {
        cout << "链表中未查询到相应的值。" << endl;
        return NULL;
    }
}

/******************************************************************
* @brief ChangeNodeByData      修改链表中相应的值
* Detail                       根据传入的参数，逐结点查询链表是否有
*                              相应的结点。查询失败返回原链表，查询
*                              成功将第一个对应参数的结点的数值修改
*
* @param[in] iSrcNodeData      需要修改的结点值
* @param[in] iChangeNodeData   修改后的数值
* @return                      
* @note
******************************************************************/
void CSingleLinkList::ChangeNodeByData(const int iSrcNodeData, const int iChangeNodeData)
{
    SLinkListNode* pFindNodeByData = FindNodeByData(iSrcNodeData);

    if (pFindNodeByData != NULL)
    {
        pFindNodeByData->iNodeData = iChangeNodeData;
        cout << "修改链表数值成功。" << endl;
    }
    else
    {
        cout << "链表中无对应值，无法修改。" << endl;
    }
}