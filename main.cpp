#include "SingleLinkList.h"

int main(int argc, char* argv[])
{
	CSingleLinkList cLinkList;

	for (int i = 1; i < 4; i++)
	{
		cLinkList.InsertHeadNode(i);
		cLinkList.InsertTailNode(i);
		cLinkList.PrintLinkList();
	}

	cLinkList.DelHeadNode();
	cLinkList.PrintLinkList();

	cLinkList.DelTailNode();
	cLinkList.PrintLinkList();

	cLinkList.ChangeNodeByData(2, 4);
	cLinkList.PrintLinkList();

	return 0;
}