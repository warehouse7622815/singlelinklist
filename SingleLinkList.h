/******************************************************************
* @file       SingleLinkList.h         
* @brief                          对单链表实现增删查改
* -----------------------------------------------------------------
* Detail                          对单链表实行初始化、打印输出、增
*                                （头插、尾插）、删（头删、尾删）、
*                                 按值查询、修改功能
*------------------------------------------------------------------
* @pversion
* @date       2024/4/2
* @author     张钰琪
* @email      564737727@qq.com
******************************************************************/
#ifndef SINGLELINKLIST_H

typedef struct SLinkListNode
{
    int                      iNodeData;
    struct SLinkListNode* pNextNode;
}sLinkListNode, * pLinkList;

/******************************************************************
* @brief CSingleLinkList           对单链表增删查改
* Detail                           类中有对单链表实行初始化、打印输
*                                  出、增（头插、尾插）、删（头删、
*                                  尾删）、按值查询、修改功能的函数
******************************************************************/
class CSingleLinkList
{
public:
    CSingleLinkList();
    ~CSingleLinkList();    

    void           PrintLinkList();
    void           InsertHeadNode(const int iNodeData);
    void           InsertTailNode(const int iNodeData);
    void           DelHeadNode();
    void           DelTailNode();
    SLinkListNode* FindNodeByData(const int iFindData);
    void           ChangeNodeByData(const int iFindNodeData, const int iChangeNodeData);

private:
    //无需外部调用，设为私有
    void InitLinkList();
    void Destroy();

    sLinkListNode* m_pHeadNode;
};

#endif